function [ output_args ] = stackWAMData(basefilename,tot)
%STACKWAMDATA Summary of this function goes here
%   Detailed explanation goes here


for i=1:tot
	filename = strcat(basefilename,num2str(i),'.mat');
	%filename = strcat('data/Parametric',num2str(i),'.mat');
	load(filename,'DataS');
	DataTime(:,:,i) = DataS(:,1)';
	DataTask(:,:,i) = DataS(:,[2:8])';
	DataJoint(:,:,i) = DataS(:,[9:15])';
end

save('data/WAMParametricAll.mat','DataTime','DataJoint','DataTask');

end

