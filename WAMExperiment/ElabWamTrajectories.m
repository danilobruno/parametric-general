function [ output_args ] = ElabWamTrajectories( input_args )
%ELABWAMTRAJECTORIES Summary of this function goes here
%   Detailed explanation goes here

load('data/Trajectories.mat','JTraj');

nbVar = size(JTraj,1);
nbData = size(JTraj,2);
nbSamples = size(JTraj,3);

basefilename = 'data/Trajs/WamTraj';

for i=1:nbSamples
	filename = strcat(basefilename,num2str(i),'.csv');
	csvwrite(filename,JTraj(:,:,i)');
end

end

