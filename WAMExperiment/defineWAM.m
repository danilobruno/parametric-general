%Initialize Barrett-WAM robot
alpha=[-pi/2 pi/2 -pi/2 pi/2 -pi/2 pi/2 0];
a = [0 0 0.045 -0.045 0 0 0];
d = [0 0 0.55 0 0.3 0 0.06];

clear L

L(1)=Link([0 d(1) a(1) alpha(1) 0 ],'standard');
L(2)=Link([0 d(2) a(2) alpha(2) 0 ],'standard');
L(3)=Link([0 d(3) a(3) alpha(3) 0 ],'standard');
L(4)=Link([0 d(4) a(4) alpha(4) 0 ],'standard');
L(5)=Link([0 d(5) a(5) alpha(5) 0 ],'standard');
L(6)=Link([0 d(6) a(6) alpha(6) 0 ],'standard');
L(7)=Link([0 d(7) a(7) alpha(7) 0 ],'standard');

% limitations
L(1).qlim = [-2.6 2.6];
L(2).qlim = [-2 2];
L(3).qlim = [-2.8 2.8];
L(4).qlim = [-0.9 3.1];
L(5).qlim = [-4.76 1.24];
L(6).qlim = [-1.6 1.6];
L(7).qlim = [-3 3];

%Create robot
wam = SerialLink(L,'name','WAM arm','comment','7DOF');

%Initialize wam position in joint space

%Rest vertical position
q0 = zeros(1,7);

set(0,'DefaultLineLineWidth',2)
