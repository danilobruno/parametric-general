function [ output_args ] = demoParametric( input_args )
%DEMOPARAMETRIC Summary of this function goes here
%   Detailed explanation goes here

needsData = 1;
needsFigure = 1;

Parameters.nbExplore = 20;
Parameters.nbStatesTask = 3;
Parameters.nbStatesModel = 2;

Param.kP = 10;
Param.kV = 6.32;
Param.alpha = 1;
Parameters.Param = Param;

Parameters.nbVarJoint = 5;

nbFinal = 1;
close all


%%Set parameters
%
%%Collect Data
%This function loads data from file and gives back the data in tensor form
%Data has the following format:
%D: dimension N: number of datapoints P:number of samples
if needsData == 1 
	[Data,nbVar,nbPar,Par,Parameters] = ElabData(Parameters);
	save('data/dataDemoTensor.mat','Data','nbVar','nbPar','Par','Parameters');
end
load('data/dataDemoTensor.mat','Data','nbVar','nbPar','Par','Parameters');

%Data1 = reshape(Data,nbVar+nbPar,nbData*nbSamples);
%%Create model of task
%
[PRIORS,MU,SIGMA,Par] = ModelTask(Data,nbVar,nbPar,Parameters);
Parameters.PRIORS = PRIORS;
%%Summarize model into a single point
nbSamples = size(MU,3);
for i=1:nbSamples
	Y(:,i) = GMMtoVector(PRIORS,MU([1:nbVar],:,i),SIGMA([1:nbVar],[1:nbVar],:,i));
end
%%Create model of parameter distribution
%
modelPar = ModelParameters(Par);
%%Create joint model
Z = [Par;Y];
modelTot = ModelJoint(Z,Parameters);

%%Explore new parameters and add to statistics
for i=1:Parameters.nbExplore
	pars = samplePar(modelPar);
	[xx,Ytmp] = runTask(pars,modelTot,Parameters,0);
	parsTmp = getPars(xx);
	ZTmp = [parsTmp;Ytmp];
	Z = [Z ZTmp];
	modelTot = ModelJoint(Z,Parameters);
if needsFigure == 2
	plot3(xx(1,:),xx(2,:),xx(3,:),'color',[1 0 0],'Linewidth',2);hold on
	plot3(pars(1),pars(2),pars(3),'o','color','green');
	plot3(parsTmp(1),parsTmp(2),parsTmp(3),'x','color','green');
	pause
	hold off
end
end
figure
for i=1:nbFinal
	pars = samplePar(modelPar);
	[xx,Ytmp] = runTask(pars,modelTot,Parameters,0);
	parsTmp = getPars(xx);
	if needsFigure == 1
		plot3(xx(1,:),xx(2,:),xx(3,:));hold on
		plot3(pars(1),pars(2),pars(3),'o');
		plot3(parsTmp(1),parsTmp(2),parsTmp(3),'x');
		[xx,Ytmp] = runTask(pars,modelTot,Parameters,1);
		pause
		hold off
	end
end


end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Functions to implement that are specific for the current task TODO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Data,nbVar,nbPar,Par,Parameters] = ElabData(Parameters)

	load('data/Peg20122013LAll.mat','DataTime','DataJoint','DataTask');
	selSamples = [1:size(DataJoint,3)];
	Param = Parameters.Param;
	%Parameters.q0 = DataJoint(:,1,1); 
	Parameters.q0 = [-0.0339 -0.6013 0.0462 2.7887 -0.0354 -0.0772 -0.0538]';
	nbVar = size(DataJoint,1)+1;
	%nbVar = Parameters.nbVarJoint; %Only select first 4 joints
	nbData = size(DataJoint,2);
	%nbSamples = size(DataJoint,3);
	nbSamples = size(selSamples,2);
	%DataTime = DataTime(:,:,[1:nbSamples]);
	%DataJoint = DataJoint(:,:,[1:nbSamples]);
	%DataTask = DataTask(:,:,[1:nbSamples]);
	DataTime = DataTime(:,:,selSamples);
	DataJoint = DataJoint(:,:,selSamples);
	DataTask = DataTask(:,:,selSamples);

	%Elaborate data
	%Select only relevant dimensions for Data (nbVar)
	%Assign to each datapoint the corresponding values of the parameters (nbPar)
	DataSel = zeros(nbVar,nbData,nbSamples);
	%DataSel([2:nbVar],:,:) = DataJoint;
	VelJoint = zeros(nbVar-1,nbData);
	AccJoint = zeros(nbVar-1,nbData);
	for i=1:nbSamples
		%Bring all demonstrations to the duration of 5 seconds
		DataSel(1,:,i) = DataTime(:,:,i) - repmat(DataTime(1,1,i),1,nbData);
		Duration = DataSel(1,end,i)-DataSel(1,1,i);
		DataSel(1,:,i) = DataSel(1,:,i)/Duration*5;
		for j=2:nbData
			VelJoint(:,j) = (DataJoint(:,j,i) - DataJoint(:,j-1,i))/(DataSel(1,j,i)-DataSel(1,j-1,i)); 
			AccJoint(:,j) = (VelJoint(:,j) - VelJoint(:,j-1))/(DataSel(1,j,i)-DataSel(1,j-1,i)); 
		end
		DataSel([2:nbVar],:,i) = 1/Param.kP*(AccJoint(:,:) + Param.kP*DataJoint(:,:,i) + Param.kV*VelJoint(:,:));
		%Param.dt = (DataTime(:,end,1) - DataTime(:,1,1))/nbData;
		Param.dt = 5/nbData;
	end
	for i=1:nbSamples
		Par(:,i) = getPars(DataTask([1:3],:,i));
		%Stack data and parameters together
		Data(:,:,i) = [DataSel(:,:,i);repmat(Par(:,i),1,nbData)];
	end
	nbVar = size(DataSel,1);
	nbPar = size(Par,1);
	Parameters.Param = Param;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Y = GMMtoVector(PRIORS,MU,SIGMA)
	nbStates = size(SIGMA,3);
	nbVar = size(MU,1);
	Y = reshape(MU([1:nbVar],:),nbVar*nbStates,1);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function model = VectortoGMM(Y,Parameters)
	nbStates = Parameters.nbStatesTask;
	for i=1:nbStates
		model.Sigma(:,:,i) = 0.1*eye(Parameters.nbVarJoint);
	end
	model.Priors = Parameters.PRIORS;
	nbVar = Parameters.nbVarJoint;
	model.Mu = reshape(Y,nbVar,nbStates);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function modelPar = ModelParameters(Par)
	modelPar.Mu(:,1) = mean(Par([1:3],:)')';
	modelPar.Mu(:,2) = mean(Par([4:6],:)')';
	modelPar.Sigma(:,:,1) = cov(Par([1:3],:)');
	modelPar.Sigma(:,:,2) = cov(Par([4:6],:)');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function pars = samplePar(modelPar)
		kkk = 1;
		pars([1:3],1) = mvnrnd(modelPar.Mu(:,kkk),modelPar.Sigma(:,:,kkk))';
		kkk = 2;
		pars([4:6],1) = mvnrnd(modelPar.Mu(:,kkk),modelPar.Sigma(:,:,kkk))';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Tip,Ytmp] = runTask(pars,modelTot,Parameters,needsPlot)
	defineWAM;
	T = wam.fkine(Parameters.q0);
	Tip = T([1:3],4);
	%%Linear trajectory to initial position
	tt = linspace(0,1,100);
	Path = repmat(Tip,1,100) + (pars([1:3],1) - Tip)*tt;
	[~,JJ] = runOnWam(Path,0);
	q0 = JJ(:,end);
	nbPar = size(pars,1);
	gmmTot = GMR(pars,[1:nbPar],[nbPar+1:modelTot.nbVar],modelTot); %DEBUG
	GMM = gmmTot.Mu;
	Ytmp = GMM;
	modelRun = VectortoGMM(GMM,Parameters);
	Param = Parameters.Param;
	out = dmpSprings(modelRun.Mu,modelRun.Sigma,q0,Param);
	xx = out.Data([1:4],:);
	q = repmat(Parameters.q0,1,size(xx,2));
	for jj=1:size(xx,2)
		q([1:4],jj) = xx(:,jj);
		T = wam.fkine(q(:,jj)');
		Tip(:,jj) = T([1:3],4);
	end
	if needsPlot == 1
		wam.plot(q');
	end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function parsTmp = getPars(xx)
	parsTmp([1:3],1) = xx([1:3],end); %Final point
	parsTmp([4:6],1) = xx([1:3],end); %Final point
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Other functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Functions implementing the general algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


function [PRIORS,MU,SIGMA,Par,Parameters] = ModelTask(Data,nbVar,nbPar,Parameters)
	nbVarAll = nbVar+nbPar;
	nbData = size(Data,2);
	nbSamples = size(Data,3);
	DataTensor = zeros(nbVarAll,nbData,1,nbSamples);
	for i=1:nbSamples
		DataTensor(:,:,1,i) = Data([1:nbVar,nbVar+1:end],:,i);
	end

	nbVarAll = nbVar+nbPar;
	%Shape data for EM
	%Data1 = reshape(Data,nbVar+nbPar,nbData*nbSamples);

	%Use tensor GMM all reproductions are stack together in a tensor 
	%dim1 - variables
	%dim2 - Datapoints (length)
	%dim3 - nbDemo for each parameter value (1)
	%dim4 - Demonstrations for different values of parameters
	%All the different values of the parameters have in common the same Priors
	nbStates = Parameters.nbStatesTask;
	model.nbVar = nbVarAll;
	model.nbPar = nbPar;
	model.nbStates = nbStates;
	model.nbFrames = nbSamples;
	model.nbSamples = 1;
	model = init_tensorGMM_timeBased(DataTensor, model);
	model = EM_tensorGMM(DataTensor,model);
	for sample = 1:nbSamples
		Par(:,sample) = Data([nbVar+1:nbVar+nbPar],1,sample);
	end
	PRIORS = model.Priors;
	MU = model.Mu;
	SIGMA = model.Sigma;
end

function modelTot = ModelJoint(Z,Parameters)

	modelTot.nbStates = Parameters.nbStatesModel;
	modelTot.nbVar = size(Z,1);
	modelTot.nbPoints = size(Z,2);
	[PriorsTmp,MuTmp,SigmaTmp] = EM_init_kmeans(Z,modelTot.nbStates);
	[PriorsTmp,MuTmp,SigmaTmp,logLik] = EM(Z,PriorsTmp,MuTmp,SigmaTmp);
	modelTot.Mu = MuTmp;
	modelTot.Sigma = SigmaTmp;
	modelTot.Priors = PriorsTmp;
	modelTot.Likelihood = logLik;
	%F = gaussPDF(Z,modelTot.Mu,modelTot.Sigma);
	%modelTot.Likelihood = mean(log(F));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Functions needed by the general algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%

function [Priors, Mu, Sigma, Pix] = EM(Data, Priors0, Mu0, Sigma0)
%
% Expectation-Maximization estimation of GMM parameters.
% This source code is the implementation of the algorithms described in 
% Section 2.6.1, p.47 of the book "Robot Programming by Demonstration: A 
% Probabilistic Approach".
%
% Author:	Sylvain Calinon, 2009
%			http://programming-by-demonstration.org
%
% This function learns the parameters of a Gaussian Mixture Model 
% (GMM) using a recursive Expectation-Maximization (EM) algorithm, starting 
% from an initial estimation of the parameters.
%
%
% Inputs -----------------------------------------------------------------
%   o Data:    D x N array representing N datapoints of D dimensions.
%   o Priors0: 1 x K array representing the initial prior probabilities 
%              of the K GMM components.
%   o Mu0:     D x K array representing the initial centers of the K GMM 
%              components.
%   o Sigma0:  D x D x K array representing the initial covariance matrices 
%              of the K GMM components.
% Outputs ----------------------------------------------------------------
%   o Priors:  1 x K array representing the prior probabilities of the K GMM 
%              components.
%   o Mu:      D x K array representing the centers of the K GMM components.
%   o Sigma:   D x D x K array representing the covariance matrices of the 
%              K GMM components.
%
% This source code is given for free! However, I would be grateful if you refer 
% to the book (or corresponding article) in any academic publication that uses 
% this code or part of it. Here are the corresponding BibTex references: 
%
% @book{Calinon09book,
%   author="S. Calinon",
%   title="Robot Programming by Demonstration: A Probabilistic Approach",
%   publisher="EPFL/CRC Press",
%   year="2009",
%   note="EPFL Press ISBN 978-2-940222-31-5, CRC Press ISBN 978-1-4398-0867-2"
% }
%
% @article{Calinon07,
%   title="On Learning, Representing and Generalizing a Task in a Humanoid Robot",
%   author="S. Calinon and F. Guenter and A. Billard",
%   journal="IEEE Transactions on Systems, Man and Cybernetics, Part B",
%   year="2007",
%   volume="37",
%   number="2",
%   pages="286--298",
% }


%% Criterion to stop the EM iterative update
loglik_threshold = 1e-10;

%% Initialization of the parameters
[nbVar, nbData] = size(Data);
nbStates = size(Sigma0,3);
loglik_old = -realmax;
nbStep = 0;

Mu = Mu0;
Sigma = Sigma0;
Priors = Priors0;

%% EM fast matrix computation (see the commented code for a version 
%% involving one-by-one computation, which is easier to understand)
while 1
  %% E-step %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for i=1:nbStates
    %Compute probability p(x|i)
    Pxi(:,i) = gaussPDF(Data, Mu(:,i), Sigma(:,:,i));
  end
  %Compute posterior probability p(i|x)
  Pix_tmp = repmat(Priors,[nbData 1]).*Pxi;
  Pix = Pix_tmp ./ repmat(sum(Pix_tmp,2),[1 nbStates]);
  %Compute cumulated posterior probability
  E = sum(Pix);
  %% M-step %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  for i=1:nbStates
    %Update the priors
    Priors(i) = E(i) / nbData;
    %Update the centers
    Mu(:,i) = Data*Pix(:,i) / E(i);
    %Update the covariance matrices
    Data_tmp1 = Data - repmat(Mu(:,i),1,nbData);
    Sigma(:,:,i) = (repmat(Pix(:,i)',nbVar, 1) .* Data_tmp1*Data_tmp1') / E(i);
    %% Add a tiny variance to avoid numerical instability
    Sigma(:,:,i) = Sigma(:,:,i) + 1E-10.*diag(ones(nbVar,1));
  end
  %% Stopping criterion %%%%%%%%%%%%%%%%%%%%
  for i=1:nbStates
    %Compute the new probability p(x|i)
    Pxi(:,i) = gaussPDF(Data, Mu(:,i), Sigma(:,:,i));
  end
  %Compute the log likelihood
  F = Pxi*Priors';
  F(find(F<realmin)) = realmin;
  loglik = mean(log(F));
  %Stop the process depending on the increase of the log likelihood 
  if abs((loglik/loglik_old)-1) < loglik_threshold
    break;
  end
  loglik_old = loglik;
  nbStep = nbStep+1;
end

% %% EM slow one-by-one computation (better suited to understand the
% %% algorithm) 
% while 1
%   %% E-step %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   for i=1:nbStates
%     %Compute probability p(x|i)
%     Pxi(:,i) = gaussPDF(Data, Mu(:,i), Sigma(:,:,i));
%   end
%   %Compute posterior probability p(i|x)
%   for j=1:nbData
%     Pix(j,:) = (Priors.*Pxi(j,:))./(sum(Priors.*Pxi(j,:))+realmin);
%   end
%   %Compute cumulated posterior probability
%   E = sum(Pix);
%   %% M-step %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   for i=1:nbStates
%     %Update the priors
%     Priors(i) = E(i) / nbData;
%     %Update the centers
%     Mu(:,i) = Data*Pix(:,i) / E(i);
%     %Update the covariance matrices 
%     covtmp = zeros(nbVar,nbVar);
%     for j=1:nbData
%       covtmp = covtmp + (Data(:,j)-Mu(:,i))*(Data(:,j)-Mu(:,i))'.*Pix(j,i);
%     end
%     Sigma(:,:,i) = covtmp / E(i);
%   end
%   %% Stopping criterion %%%%%%%%%%%%%%%%%%%%
%   for i=1:nbStates
%     %Compute the new probability p(x|i)
%     Pxi(:,i) = gaussPDF(Data, Mu(:,i), Sigma(:,:,i));
%   end
%   %Compute the log likelihood
%   F = Pxi*Priors';
%   F(find(F<realmin)) = realmin;
%   loglik = mean(log(F));
%   %Stop the process depending on the increase of the log likelihood 
%   if abs((loglik/loglik_old)-1) < loglik_threshold
%     break;
%   end
%   loglik_old = loglik;
%   nbStep = nbStep+1;
% end

%% Add a tiny variance to avoid numerical instability
for i=1:nbStates
  Sigma(:,:,i) = Sigma(:,:,i) + 1E-5.*diag(ones(nbVar,1));
end

end

function [Priors, Mu, Sigma] = EM_init_kmeans(Data, nbStates)
%
% This function initializes the parameters of a Gaussian Mixture Model 
% (GMM) by using k-means clustering algorithm.
%
% Author:	Sylvain Calinon, 2009
%			http://programming-by-demonstration.org
%
% Inputs -----------------------------------------------------------------
%   o Data:     D x N array representing N datapoints of D dimensions.
%   o nbStates: Number K of GMM components.
% Outputs ----------------------------------------------------------------
%   o Priors:   1 x K array representing the prior probabilities of the
%               K GMM components.
%   o Mu:       D x K array representing the centers of the K GMM components.
%   o Sigma:    D x D x K array representing the covariance matrices of the 
%               K GMM components.
% Comments ---------------------------------------------------------------
%   o This function uses the 'kmeans' function from the MATLAB Statistics 
%     toolbox. If you are using a version of the 'netlab' toolbox that also
%     uses a function named 'kmeans', please rename the netlab function to
%     'kmeans_netlab.m' to avoid conflicts. 

[nbVar, nbData] = size(Data);

%Use of the 'kmeans' function from the MATLAB Statistics toolbox
[Data_id, Centers] = kmeans(Data', nbStates); 
Mu = Centers';
for i=1:nbStates
  idtmp = find(Data_id==i);
  Priors(i) = length(idtmp);
  Sigma(:,:,i) = cov([Data(:,idtmp) Data(:,idtmp)]');
  %Add a tiny variance to avoid numerical instability
  Sigma(:,:,i) = Sigma(:,:,i) + 1E-5.*diag(ones(nbVar,1));
end
Priors = Priors ./ sum(Priors);

end

function [model, LL] = EM_tensorGMM(Data, model)
% Author:	Sylvain Calinon, 2013
%         http://programming-by-demonstration.org/SylvainCalinon
%
% Code to study the extension of GMM to tensorial data

%Parameters of the EM algorithm
nbMinSteps = 15; %Minimum number of iterations allowed
nbMaxSteps = 25; %Maximum number of iterations allowed
maxDiffLL = 1E-8; %Likelihood increase threshold to stop the algorithm
[nbVar,nbData,nbSamples,nbFrames] = size(Data);

for nbIter=1:nbMaxSteps
  fprintf('.');
  %E-step
  GAMMA = computeGamma(Data, model); %See 'computeGamma' function below 
  %M-step
  for i=1:model.nbStates 
    %Update Priors
    model.Priors(i) = sum(sum(GAMMA(i,:,:))) / (nbData*nbSamples);
    for m=1:nbFrames
      %Matricization/flattening of tensors
      DataMat = double(tenmat(Data(:,:,:,m),1));
      GammaMat = double(tenmat(GAMMA(i,:,:),1)) / sum(double(tenmat(GAMMA(i,:,:),1)));
      %Update Mu 
      model.Mu(:,i,m) = DataMat * GammaMat';
      %Update Sigma (regularization term is optional) 
      DataTmp = DataMat - repmat(model.Mu(:,i,m),1,nbData*nbSamples);
      model.Sigma(:,:,i,m) = DataTmp * diag(GammaMat) * DataTmp' + eye(nbVar)*2E-5;
    end
  end
  %Compute average log-likelihood 
  GammaMat = double(tenmat(GAMMA,1)); %Matricization/flattening of tensor
  LL(nbIter) = sum(log(sum(GammaMat,1))) / size(GammaMat,2);
  %Stop the algorithm if EM converged (small change of LL)
  if nbIter>nbMinSteps
    if LL(nbIter)-LL(nbIter-1)<maxDiffLL || nbIter==nbMaxSteps-1
      disp(['EM converged after ' num2str(nbIter) ' iterations.']); 
      return;
    end
  end
end
disp(['The maximum number of ' num2str(nbMaxSteps) ' EM iterations has been reached.']); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function GAMMA = computeGamma(Data, model)
  [nbVar,nbData,nbSamples,nbFrames] = size(Data);
  %Gaussian product
  GAMMA = ones(model.nbStates,nbData*nbSamples);
  for m=1:nbFrames
    DataMat = double(tenmat(Data(:,:,:,m),1)); %Matricization/flattening of tensor
    for i=1:model.nbStates
      %GAMMA(i,:) = GAMMA(i,:) .* gaussPDF(DataMat, model.Mu(:,i,m), model.Sigma(:,:,i,m))'; 
      GAMMA(i,:) = GAMMA(i,:) .* (model.Priors(i)*gaussPDF(DataMat, model.Mu(:,i,m), model.Sigma(:,:,i,m)))'; 
    end
  end
  %Normalization
  GAMMA = GAMMA ./ repmat(sum(GAMMA,1)+realmin,size(GAMMA,1),1);
  %Data reshape
  GAMMA = reshape(GAMMA,[model.nbStates,nbData,nbSamples]);
end


function model = init_tensorGMM_timeBased(Data, model)
% Initialization of the model with clusters equally split in time.
%
% Author:	Sylvain Calinon, 2013
%         http://programming-by-demonstration.org/SylvainCalinon

diagRegularizationFactor = 1E-4;
nbSamples = size(Data,3);

DataAll = double(tenmat(Data,[1,4])); %Matricization/flattening of tensor
size(DataAll);

TimingSep = linspace(min(DataAll(1,:)), max(DataAll(1,:)), model.nbStates+1);
for i=1:model.nbStates
  idtmp = find( DataAll(1,:)>=TimingSep(i) & DataAll(1,:)<TimingSep(i+1));
  Mu(:,i) = mean(DataAll(:,idtmp)');
  Sigma(:,:,i) = cov(DataAll(:,idtmp)') + eye(size(DataAll,1))*diagRegularizationFactor;
  model.Priors(i) = length(idtmp);
end
model.Priors = model.Priors / sum(model.Priors);

%Reshape data
for m=1:model.nbFrames
  for i=1:model.nbStates
    model.Mu(:,i,m) = Mu((m-1)*model.nbVar+1:m*model.nbVar,i); 
    model.Sigma(:,:,i,m) = Sigma((m-1)*model.nbVar+1:m*model.nbVar,(m-1)*model.nbVar+1:m*model.nbVar,i); 
  end
end

end

function prob = gaussPDF(Data, Mu, Sigma)
% Likelihood of datapoint(s) to be generated by a Gaussian represented by means and covariance matrix.
%
% Inputs -----------------------------------------------------------------
%   o Data:  D x N array representing N datapoints of D dimensions.
%   o Mu:    D x K array representing the centers of the K GMM components.
%   o Sigma: D x D x K array representing the covariance matrices of the K GMM components.
% Outputs ----------------------------------------------------------------
%   o prob:  1 x N array representing the probabilities for the N datapoints.     
%
% Author:	Sylvain Calinon, 2012
%         http://programming-by-demonstration.org/SylvainCalinon

[nbVar,nbData] = size(Data);

Data = Data' - repmat(Mu',nbData,1);
prob = sum((Data*inv(Sigma)).*Data, 2);
prob = exp(-0.5*prob) / sqrt((2*pi)^nbVar * (abs(det(Sigma))+realmin));

end


function rGMR = GMR(x,in,out,model)

nbData = size(x,2);

Mu = model.Mu;
Sigma = model.Sigma;
Priors = model.Priors;

nbVar = size(Mu,1);
nbStates = size(Sigma,3);

rGMR.Mu = zeros(length(out),nbData);
rGMR.Sigma = zeros(length(out),length(out),nbData);
rGMR.Wp = zeros(length(out),length(out),nbData);
rGMR.H = zeros(nbStates,nbData);

for n=1:nbData
	for i=1:nbStates
		rGMR.H(i,n) = Priors(i) * gaussPDF(x(:,n),Mu(in,i),Sigma(in,in,i));
	end
	rGMR.H(:,n) = rGMR.H(:,n) / sum(rGMR.H(:,n));
	for i=1:nbStates
		MuTmp(:,i) = Mu(out,i) + Sigma(out,in,i)*inv(Sigma(in,in,i))*(x(:,n) - Mu(in,i));
		rGMR.Mu(:,n) = rGMR.Mu(:,n) + MuTmp(:,i) * rGMR.H(i,n);
		rGMR.Sigma(:,:,n) = rGMR.Sigma(:,:,n) + rGMR.H(i,n)^2 .* (Sigma(out,out,i) - (Sigma(out,in,i)*inv(Sigma(in,in,i))*Sigma(in,out,i)));
	end
	rGMR.Wp(:,:,n) = inv(rGMR.Sigma(:,:,n));
end


end


function h = plotGMM(Mu, Sigma, color, display_mode, valAlpha);
%
% This function plots a representation of the components (means and 
% covariance amtrices) of a Gaussian Mixture Model (GMM) or a
% Gaussian Mixture Regression (GMR).
%
% Inputs -----------------------------------------------------------------
%   o Mu:           D x K array representing the centers of the K GMM components.
%   o Sigma:        D x D x K array representing the covariance matrices of the 
%                   K GMM components.
%   o color:        Color used for the representation
%   o display_mode: Display mode (1 is used for a GMM, 2 is used for a GMR
%                   with a 2D representation and 3 is used for a GMR with a
%                   1D representation).
%
% Copyright (c) 2006 Sylvain Calinon, LASA Lab, EPFL, CH-1015 Lausanne,
%               Switzerland, http://lasa.epfl.ch

nbData = size(Mu,2);
lightcolor = min(color+0.3,1);
nbDrawingSeg = 35;

if display_mode==1  
  t = linspace(-pi, pi, nbDrawingSeg)';
  for j=1:nbData
    %stdev = sqrtm(5.0.*Sigma(:,:,j));
    stdev = sqrtm(1.0.*Sigma(:,:,j));
    X = [cos(t) sin(t)] * real(stdev) + repmat(Mu(:,j)',nbDrawingSeg,1);
    %patch(X(:,1), X(:,2), lightcolor, 'lineWidth', 1, 'EdgeColor', color);
    h(1)=plot(X(:,1), X(:,2),'-', 'color', color);
    h(2)=plot(Mu(1,:), Mu(2,:), '.', 'lineWidth', 2, 'markersize', 6, 'color', color);
  end
elseif display_mode==2
  t = linspace(-pi, pi, nbDrawingSeg)';
  for j=1:nbData
    stdev = sqrtm(1.0.*Sigma(:,:,j));
    X = [cos(t) sin(t)] * real(stdev) + repmat(Mu(:,j)',nbDrawingSeg,1);
    patch(X(:,1), X(:,2), lightcolor, 'edgecolor', color, 'facealpha',valAlpha,'edgealpha',valAlpha);
  end
  %plot(Mu(1,:), Mu(2,:), '.', 'markersize', 6, 'color', color); %min(color+0.1,1)
  for j=1:nbData
    X = [cos(t) sin(t)] * 0.006 + repmat(Mu(:,j)',nbDrawingSeg,1);
    patch(X(:,1), X(:,2), color, 'LineStyle', 'none', 'facealpha',valAlpha);
  end  
elseif display_mode==3
  for j=1:nbData
    ymax(j) = Mu(2,j) + sqrt(3.*Sigma(1,1,j));
    ymin(j) = Mu(2,j) - sqrt(3.*Sigma(1,1,j));
  end
  patch([Mu(1,1:end) Mu(1,end:-1:1)], [ymax(1:end) ymin(end:-1:1)], lightcolor, 'LineStyle', 'none');
  plot(Mu(1,:), Mu(2,:), '-', 'lineWidth', 2, 'color', color); 
elseif display_mode==4
  t = linspace(-pi, pi, nbDrawingSeg)';
  for j=1:nbData
    stdev = sqrtm(Sigma(:,:,j));
    X = [cos(t) sin(t)] * real(stdev) + repmat(Mu(:,j)',nbDrawingSeg,1);
    patch(X(:,1), X(:,2), lightcolor, 'lineWidth', 1, 'EdgeColor', color,'facealpha',valAlpha,'edgealpha',valAlpha);
    %plot(X(:,1), X(:,2),'-', 'color', color);
    plot(Mu(1,:), Mu(2,:), 'x', 'lineWidth', 1, 'color', color);
  end
elseif display_mode==5
  t = linspace(-pi, pi, nbDrawingSeg)';
  for j=1:nbData
    stdev = sqrtm(5.0.*Sigma(:,:,j));
    X = [cos(t) sin(t)] * real(stdev) + repmat(Mu(:,j)',nbDrawingSeg,1);
    patch(X(:,1), X(:,2), lightcolor, 'lineWidth', 1, 'EdgeColor', color);
    %plot(X(:,1), X(:,2),'-', 'color', color,'linewidth',1.5);
    plot(Mu(1,:), Mu(2,:), 'x', 'lineWidth', 1.5, 'color', color);
  end
elseif display_mode==6
  t = linspace(-pi, pi, nbDrawingSeg)';
  for j=1:nbData
    stdev = sqrtm(1.0.*Sigma(:,:,j));
    X = [cos(t) sin(t)] * real(stdev) + repmat(Mu(:,j)',nbDrawingSeg,1);
    patch(X(:,1), X(:,2), lightcolor, 'edgecolor', color, 'facealpha',valAlpha,'edgealpha',valAlpha);
  end
end

end

function h=plotGMM3D(Mu, Sigma, col1,alpha)

nbData = size(Mu,2);
nbPoints = 20; %nb of points to form a circular path
nbRings = 10; %Number of circular paths following the principal direction

pts0 = [cos(linspace(0,2*pi,nbPoints)); sin(linspace(0,2*pi,nbPoints))];

for n=1:nbData
  [V0,D0] = eigs(Sigma(:,:,n));
  U0 = V0*D0^.5;

  ringpts0 = [cos(linspace(0,pi,nbRings+1)); sin(linspace(0,pi,nbRings+1))];
  ringpts = zeros(3,nbRings); 
  ringpts([2,3],:) = ringpts0(:,1:nbRings);
  U = zeros(3); 
  U(:,[2,3]) = U0(:,[2,3]); 
  ringTmp = U*ringpts; 
 
  %Compute touching circular paths
  for j=1:nbRings
    U = zeros(3); 
    U(:,1) = U0(:,1); 
    U(:,2) = ringTmp(:,j);
    pts = zeros(3,nbPoints); 
    pts([1,2],:) = pts0;
    xring(:,:,j) = U*pts + repmat(Mu(:,n),1,nbPoints);
  end
	h = [];
  %Plot filled ellispoid
  xringfull = xring;
  xringfull(:,:,end+1) = xringfull(:,end:-1:1,1); %Close the ellipsoid
  for j=1:size(xringfull,3)-1
    for i=1:size(xringfull,2)-1
      xTmp = [xringfull(:,i,j) xringfull(:,i+1,j) xringfull(:,i+1,j+1) xringfull(:,i,j+1) xringfull(:,i,j)];     
      h = [h patch(xTmp(1,:),xTmp(2,:),xTmp(3,:),min(col1+0.6,1),'edgecolor',col1,'linewidth',0.1,'facealpha',alpha,'edgealpha',alpha)]; %,'facealpha',0.5
    end
  end
end

end
