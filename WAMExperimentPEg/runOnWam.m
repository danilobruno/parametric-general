function [ DataTask DataJoint ] = runOnWam( Data,plotRobot )
%runOnWam Runs a trajectory in task space on the barrett wam 
%			using the robotics toolbox
%
%			Only controls the trajectory of the tip 
%
%Author: Danilo Bruno (2013)

%INPUT:
%
%	Data		3 x N Input trajectory in task space
%	plotRobot	1 : plots the robot
%
%OUTPUT
%	DataTask 	3 x N Output in task space
%	DataJoint	7 x N Output in joint space

%Total number of points for IK between task points
Tot = 5;
nbVar = 3;

X = Data([1:nbVar],[1:end]);
X0 = Data([1:nbVar],1);
nbData = size(X,2);



%Initialize Barrett-WAM robot
alpha=[-pi/2 pi/2 -pi/2 pi/2 -pi/2 pi/2 0];
a = [0 0 0.045 -0.045 0 0 0];
d = [0 0 0.55 0 0.3 0 0.06];

clear L

L(1)=Link([0 d(1) a(1) alpha(1) 0 ],'standard');
L(2)=Link([0 d(2) a(2) alpha(2) 0 ],'standard');
L(3)=Link([0 d(3) a(3) alpha(3) 0 ],'standard');
L(4)=Link([0 d(4) a(4) alpha(4) 0 ],'standard');
L(5)=Link([0 d(5) a(5) alpha(5) 0 ],'standard');
L(6)=Link([0 d(6) a(6) alpha(6) 0 ],'standard');
L(7)=Link([0 d(7) a(7) alpha(7) 0 ],'standard');

% limitations
L(1).qlim = [-2.6 2.6];
L(2).qlim = [-2 2];
L(3).qlim = [-2.8 2.8];
L(4).qlim = [-0.9 3.1];
L(5).qlim = [-4.76 1.24];
L(6).qlim = [-1.6 1.6];
L(7).qlim = [-3 3];

%Create robot
wam = SerialLink(L,'name','WAM arm','comment','7DOF');

%Initialize wam position in joint space

%Rest vertical position
q0 = zeros(1,7);

set(0,'DefaultLineLineWidth',2)
%Ready position
q0 = [ 0 -pi/3 0 2/3*pi 0 0 0];

TReady = wam.fkine(q0);
XReady = TReady([1:3],4);

%Move to initial point
V = X0-XReady;

%Evaluates movement with IK
dX = V/Tot;
q = q0;
Q = q;
for time=1:Tot
	J = wam.jacob0(q);
	J1 = J([1:3],:);
	dQ = pinv(J1)*dX;
	q = q+ dQ';
	Q = [Q;q];
end

if plotRobot == 1
	wam.plot(q,'nobase');hold on
	plot3(Data(1,:),Data(2,:),Data(3,:),'color','red');
	axis off
	drawnow
end

Q = [];
for i=2:nbData
	X0 = X(:,i-1);
	X1 = X(:,i);
	V = X1-X0;
	dX = V/Tot;
	for time=1:Tot
		J = wam.jacob0(q);
		J1 = J([1:3],:);
		dQ = pinv(J1)*dX;
		q = q+ dQ';
		Q = [Q;q];
	end
	Q = [Q;q];
end
M = 'movieFiles';
if plotRobot == 1 
	axis([-0.5 1 -0.5 0.5 0 0.5])
	axis manual
	wam.plot(Q,'nobase','movie',M);hold on
	drawnow
end

for i=1:size(Q,1)
	T = wam.fkine(Q(i,:));
	DataTask(:,i) = T([1:3],end);
end

if plotRobot == 1 
	plot3(DataTask(1,:),DataTask(2,:),DataTask(3,:));hold on
end

DataJoint = Q';

clear wam



end

