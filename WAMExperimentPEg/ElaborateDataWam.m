function [ output_args ] = ElaborateDataWam( filename )
%ELABORATEDATAWAM Summary of this function goes here
%   Detailed explanation goes here


DataRaw = csvread(filename);
tot = size(DataRaw,2);
for i=1:tot
	rows = ceil(tot/5);
	subplot(rows,5,i);
	plot([1:size(DataRaw,1)],DataRaw(:,i));
end
nbPoints = 200;

Start = input('Parametro iniziale : ');
End = input('Parametro finale : ');

Length = End - Start;
step = round(Length/nbPoints);
DataS = DataRaw([1:step:end],:);
DataS  =DataS([1:nbPoints],:);

filename = strcat(filename,'.mat');
save(filename,'DataS');


end

