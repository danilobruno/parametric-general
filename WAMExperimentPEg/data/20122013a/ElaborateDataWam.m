function [ output_args ] = ElaborateDataWam( filename )
%ELABORATEDATAWAM Summary of this function goes here
%   Detailed explanation goes here


DataRaw = csvread(filename);
tot = size(DataRaw,2);
%for i=1:tot
	%rows = ceil(tot/5);
	%subplot(rows,5,i);
	plot([1:size(DataRaw,1)],DataRaw(:,2));
%end
nbPoints = 200;

Start = input('Parametro iniziale : ');
End = input('Parametro finale : ');
%DataRaw = DataRaw([Start:End],:);
Length = End - Start;
step = floor(Length/nbPoints);
DataS = DataRaw([Start:step:End],:);
DataS  =DataS([1:nbPoints],:);

filename = strcat(filename,'.mat');
save(filename,'DataS');

close all

end

