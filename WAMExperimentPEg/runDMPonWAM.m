function [TrajTask TrajJoint] = runDMPonWAM(Mu,Sigma,P0,sys,plotRobot)

%Generate trajectory with DMP
curve = dmpSprings(Mu,Sigma,P0,sys);

Data = curve.Data([1:3],:);

[TrajTask TrajJoint] = runOnWam(Data,plotRobot);


end

