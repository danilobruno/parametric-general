function out = dmpSprings(Mu,Sigma,Start,Par)
%%Spring damper system implmented through a GMM
%Reproduction of motion given a GMM
%

nbData = 200; %Length of each trajectory

%kPmax = 200; %Maximum stiffness gain
%kPmin = 100; %Minimum stiffness gain
kP = Par.kP; %Initial stiffness gain
kV = Par.kV; %Damping gain
dt = Par.dt; %Time step
alpha = Par.alpha; %Decay factor



nbVar = size(Mu,1)-1; %Number of variables (Trajectory in a plane)
nbStates = size(Mu,2); %Number of states (or primitives)
%Gaussians equally distributed in time
%Mu_t = linspace(0,nbData*dt,nbStates);
%Sigma_t = (nbData*dt/nbStates)*8E-2;
Sigma_t = (nbData*dt/nbStates)*8E-1;

Mu_t = Mu(1,:);
%Sigma_t = Sigma(1,1,:);

RI = eye(nbVar,nbVar).*1E-3; %Regularization term for matrix inversion
%Fast computation
Mu_x = Mu([2:end],:);
for i=1:nbStates
	Sigma_x(:,:,i) = Sigma([2:end],[2:end],i);
	Wp(:,:,i) = inv(Sigma_x(:,:,i)+RI); %Use variation information to determine stiffness
end
currPos = Start; %Initial position
currVel = zeros(nbVar,1); %Initial velocity
s = 1; %Reinitialize the decay term
for n=1:nbData
  %Evaluate the current weights
  t = -log(s)/alpha; %Corresponding time (t=n*dt)
  for i=1:nbStates
    h(i) = gaussPDF(t,Mu_t(i),Sigma_t); %Probability to be in a given state
  end
  h = h./sum(h); %Normalization
  %Evaluate the current target and associated stiffness matrix
  currTar = zeros(nbVar,1);
  currWp = zeros(nbVar,nbVar);
  for i=1:nbStates
    currTar = currTar + Mu_x(:,i) .* h(i);
    currWp = currWp + Wp(:,:,i) .* h(i);
  end
  %Compute acceleration
  %currAcc = currWp * (currTar-currPos) - kV * currVel; %Eq.(2)
  currAcc = kP * (currTar-currPos) - kV * currVel;
  %Update veloctiy and position
  currVel = currVel + currAcc .* dt;
  currPos = currPos + currVel .* dt;
  %Update the decay term
  s = s + (-alpha*s)*dt; 
  %Keep a trace of data
  out.Data(:,n)=[currPos; currVel; currAcc];
  out.currTar(:,n)=currTar;
  out.currWp(:,:,n)=currWp;
  out.currDet(n)=det(currWp)^(1/nbVar);
end

end
